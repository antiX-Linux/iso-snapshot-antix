<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="nl">
<context>
    <name>Batchprocessing</name>
    <message>
        <location filename="batchprocessing.cpp" line="44"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="45"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Huidige kernel ondersteunt het geselecteerde compressie algoritme niet, pas a.u.b. het configuratiebestand aan en kies een ander algoritme.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MX Snapshot</source>
        <translation>MX Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Optional customization</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Boot options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Release date:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Project name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Release version:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <source>Release codename:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is een software-gereedschap waarmee een bootable  (ISO)-bestand van je werkend systeem kan worden gemaakt dat kan worden gebruikt als  opslag of distributie. Je kunt blijven doorwerken met niet al te zware applicaties terwijl mx-snapshot bezig is. &lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Gebruikte ruimte op / (root) en /home partities:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>Location and ISO name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="188"/>
        <source>Snapshot location:</source>
        <translation>Snapshot locatie:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Select a different snapshot directory</source>
        <translation>Selecteer een andere snapshot folder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="mainwindow.cpp" line="328"/>
        <source>Snapshot name:</source>
        <translation>Snapshot naam:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="mainwindow.ui" line="370"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /etc/mx-snapshot-exclude.list.</source>
        <translation>U kunt ook zekere directories uitsluiten door de algemene keuzes hieronder aan te vinken, of door op de knop te klikken om rechtstreeks /etc/mx-snapshot-exclude.list te bewerken.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="395"/>
        <source>Pictures</source>
        <translation>Afbeeldingen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Music</source>
        <translation>Muziek</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <source>All of the above</source>
        <translation>Al het bovenstaande</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>Documents</source>
        <translation>Documenten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="447"/>
        <source>Videos</source>
        <translation>Video&apos;s</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <source>exclude network configurations</source>
        <translation>netwerkconfiguraties uitsluiten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>Networks</source>
        <translation>Netwerken</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="505"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Account bewaren (voor persoonlijke backup)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="515"/>
        <source>Type of snapshot:</source>
        <translation>Type snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the MX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze optie zal &amp;quot;demo&amp;quot; en &amp;quot;root&amp;quot; wachtwoorden resetten naar de standaardwaarden van MX Linux en zal geen persoonlijke accounts die gecreëerd zijn kopieëren.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="538"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Accounts opnieuw instellen (voor distributie naar anderen)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Edit Exclusion File</source>
        <translation>Bewerk Exclusion Bestand</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="577"/>
        <source>sha512</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="584"/>
        <source>Options:</source>
        <translation>Opties:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>md5</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="611"/>
        <source>Calculate checksums:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="618"/>
        <source>ISO compression scheme:</source>
        <translation>ISO compressieschema:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="733"/>
        <source>About this application</source>
        <translation>Over deze toepassing</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <source>About...</source>
        <translation>Over...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="743"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="759"/>
        <source>Quit application</source>
        <translation>Verlaat de applicatie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="762"/>
        <source>Cancel</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="769"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="849"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="866"/>
        <source>Display help </source>
        <translation>Toon help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="869"/>
        <source>Help</source>
        <translation>Hulp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="876"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Snapshot</source>
        <translation>Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fastest, worst compression</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fast, worse compression</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>slow, better compression</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>best compromise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="166"/>
        <source>slowest, best compression</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Vrije ruimte op %1, waar de snapshot folder geplaatst is:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>De vrije ruimte zou afdoende moeten zijn om de gecomprimeerde data van / en /home te bevatten

Indien nodig kunt u meer beschikbare ruimte creëren
door oudere snapshots en opgeslagen kopieën te verwijderen:
%1 snapshots nemen %2 van de diskruimte in beslag.
 
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <location filename="mainwindow.cpp" line="207"/>
        <source>Installing </source>
        <translation>Installeren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Please wait.</source>
        <translation>Even wachten aub.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Even wachten aub. Gebruikte schijfruimte aan het berekenen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="312"/>
        <location filename="mainwindow.cpp" line="340"/>
        <location filename="mainwindow.cpp" line="369"/>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="320"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="325"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot zal de volgende instellingen gebruiken:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="327"/>
        <source>- Snapshot directory:</source>
        <translation>- Snapshot folder:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="328"/>
        <source>- Kernel to be used:</source>
        <translation>- Te gebruiken kernel:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Huidige kernel ondersteunt het geselecteerde compressie algoritme niet, pas a.u.b. het configuratiebestand aan en kies een ander algoritme.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="348"/>
        <source>Final chance</source>
        <translation>Laatste kans</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot heeft nu alle informatie die het nodig heeft om een ISO van uw lopende systeem te creëren. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Dit zal enige tijd duren, afhankelijk van de afmeting van het geïnstalleerde systeem en de capaciteit van uw computer.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>OK to start?</source>
        <translation>OK om te beginnen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Shutdown computer when done.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="369"/>
        <source>Could not create working directory. </source>
        <translation>Kon geen werkmap creëren.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Could not create temporary directory. </source>
        <translation>Kon geen tijdelijke map creëren.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="381"/>
        <source>Output</source>
        <translation>Output</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>Edit Boot Menu</source>
        <translation>Boot Menu aanpassen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Het programma zal nu pauzeren om u in staat te stellen bestanden aan te passen in de werk folder. Selecteer Ja om het opstartmenu aan te passen of selecteer Nee om deze stap over te slaan en door te gaan met het creëren van het snapshot.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>About %1</source>
        <translation>Over %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="491"/>
        <source>Version: </source>
        <translation>Versie:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Programma om een live-CD te creëren van het lopende systeem voor antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="496"/>
        <source>%1 License</source>
        <translation>%1 Licentie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>%1 Help</source>
        <translation>%1 Help</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="518"/>
        <source>Select Snapshot Directory</source>
        <translation>Selecteer Snapshot Folder</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Confirmation</source>
        <translation>Bevestiging</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Weet u zeker dat u wilt stoppen met deze applicatie?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>License</source>
        <translation>Licentie</translation>
    </message>
    <message>
        <location filename="about.cpp" line="53"/>
        <location filename="about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluiten</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="67"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Het programma zal het bouwen pauzeren en het opstartmenu openen in uw tekstverwerker.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Programma om een live-CD te creëren van het lopende systeem</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use CLI only</source>
        <translation>Gebruik enkel CLI</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>Output directory</source>
        <translation>Uitvoermap</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="100"/>
        <source>path</source>
        <translation>pad</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Output filename</source>
        <translation>Uitvoer bestandsnaam</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>name</source>
        <translation>naam</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Noem een andere kernel te gebruiken, anders dan de standaard lopende kernel, gebruik formaat opgegeven door &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Or the full path: %1</source>
        <translation>Of het volledige pad: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>version, or path</source>
        <translation>versie, of pad</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Compression level options.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Use quotes: &quot;-Xcompression-level &lt;level&gt;&quot;, or &quot;-Xalgorithm &lt;algorithm&gt;&quot;, or &quot;-Xhc&quot;, see mksquashfs man page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="86"/>
        <source>&quot;option&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="89"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Creëer een maandelijkse snapshot, voeg de &apos;Maand&apos; naam toe in de ISO naam, sla gebruikte ruimtecalculatie over</translation>
    </message>
    <message>
        <location filename="main.cpp" line="90"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Deze optie zet reset-accounts en compressie op standaard, argumenten die deze items veranderen zullen worden genegeerd</translation>
    </message>
    <message>
        <location filename="main.cpp" line="93"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Bereken geen checksums voor resulterend ISO-bestand</translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Optie om het probleem op te lossen met het berekenen van checksums op preempt_rt kernels</translation>
    </message>
    <message>
        <location filename="main.cpp" line="96"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Accounts opnieuw instellen (voor distributie naar anderen)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="97"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Bereken checksums voor resulterend ISO-bestand</translation>
    </message>
    <message>
        <location filename="main.cpp" line="99"/>
        <source>Skip calculating free space to see if the resulting ISO will fit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <source>Work directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="102"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Hoofdmappen uitsluiten, geldige keuzes:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Gebruik de optie één keer voor elk item dat u wilt uitsluiten</translation>
    </message>
    <message>
        <location filename="main.cpp" line="105"/>
        <source>one item</source>
        <translation>een item</translation>
    </message>
    <message>
        <location filename="main.cpp" line="107"/>
        <source>Compression format, valid choices: </source>
        <translation>Compressieformaat, geldige keuzes:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="108"/>
        <source>format</source>
        <translation>Formaat</translation>
    </message>
    <message>
        <location filename="main.cpp" line="109"/>
        <source>Shutdown computer when done.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="128"/>
        <location filename="main.cpp" line="138"/>
        <location filename="main.cpp" line="176"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>U lijkt ingelogd te zijn als root, gelieve uit te loggen en in te loggen als normale gebruiker om dit programma te gebruiken.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="150"/>
        <location filename="main.cpp" line="184"/>
        <source>version:</source>
        <translation>versie:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="158"/>
        <source>You must run this program as root.</source>
        <translation>U dient deze toepassing als &apos;root&apos; uit te voeren.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="167"/>
        <source>Snapshot</source>
        <translation>Snapshot</translation>
    </message>
    <message>
        <location filename="main.cpp" line="175"/>
        <location filename="main.cpp" line="259"/>
        <location filename="settings.cpp" line="222"/>
        <location filename="settings.cpp" line="235"/>
        <location filename="settings.cpp" line="633"/>
        <location filename="settings.cpp" line="723"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="main.cpp" line="253"/>
        <location filename="main.cpp" line="255"/>
        <location filename="settings.cpp" line="230"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Huidige kernel ondersteunt geen Squashfs, kan niet doorgaan.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Kon geen werkmap creëren.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="109"/>
        <source>Could not create temp directory. </source>
        <translation>Kon geen tijdelijke map creëren.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="217"/>
        <source>Could not find a usable kernel</source>
        <translation>Kon geen bruikbare kernel vinden</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="346"/>
        <source>Used space on / (root): </source>
        <translation>Gebruikte ruimte op / (root):</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>estimated</source>
        <translation>geschat</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="360"/>
        <source>Used space on /home: </source>
        <translation>Gebruikte ruimte op /home:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="411"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Vrije ruimte op %1, waar de snapshot folder geplaatst is:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="415"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>De vrije ruimte zou afdoende moeten zijn om de gecomprimeerde data van / en /home te bevatten

Indien nodig kunt u meer beschikbare ruimte creëren
door oudere snapshots en opgeslagen kopieën te verwijderen:
%1 snapshots nemen %2 van de diskruimte in beslag.
 
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="433"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="435"/>
        <source>Documents</source>
        <translation>Documenten</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="437"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="439"/>
        <source>Music</source>
        <translation>Muziek</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="441"/>
        <source>Networks</source>
        <translation>Netwerken</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="443"/>
        <source>Pictures</source>
        <translation>Afbeeldingen</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="447"/>
        <source>Videos</source>
        <translation>Video&apos;s</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="627"/>
        <location filename="settings.cpp" line="717"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="work.cpp" line="121"/>
        <source>Interrupted or failed to complete</source>
        <translation>Onderbroken of niet af kunnen maken</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="88"/>
        <source>Cleaning...</source>
        <translation>Opruimen...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="112"/>
        <location filename="work.cpp" line="120"/>
        <location filename="work.cpp" line="276"/>
        <source>Done</source>
        <translation>Klaar</translation>
    </message>
    <message>
        <location filename="work.cpp" line="151"/>
        <location filename="work.cpp" line="233"/>
        <location filename="work.cpp" line="256"/>
        <location filename="work.cpp" line="295"/>
        <location filename="work.cpp" line="411"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="work.cpp" line="152"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Er is niet genoeg vrije ruimte op uw doelschijf, u heeft minstens %1 nodig</translation>
    </message>
    <message>
        <location filename="work.cpp" line="155"/>
        <source>You have %1 free space on %2</source>
        <translation>U heeft %1 vrije ruimte op %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="157"/>
        <source>If you are sure you have enough free space rerun the program with -o/--override-size option</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="work.cpp" line="181"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>New-iso bestandssysteem kopiëren...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="193"/>
        <source>Could not create temp directory. </source>
        <translation>Kon geen tijdelijke map creëren.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="230"/>
        <source>Squashing filesystem...</source>
        <translation>Comprimeren van het bestandsysteem</translation>
    </message>
    <message>
        <location filename="work.cpp" line="234"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Kon geen linuxfs bestand creëren, controleer aub of u genoeg ruimte op de doelpartitie heeft.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="253"/>
        <source>Creating CD/DVD image file...</source>
        <translation>CD/DVD image bestand maken...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="257"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Kon geen ISO bestand creëren, controleer aub of u genoeg ruimte op de doelpartitie heeft.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="263"/>
        <source>Making hybrid iso</source>
        <translation>Hybride iso maken</translation>
    </message>
    <message>
        <location filename="work.cpp" line="281"/>
        <source>Success</source>
        <translation>Gelukt</translation>
    </message>
    <message>
        <location filename="work.cpp" line="282"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Snapshot succesvol afgerond!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="283"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Snapshot nam %1 tijd in beslag.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="284"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Bedankt voor het gebruiken van ISO Snapshot, gebruik vervolgens Live USB Maker!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="292"/>
        <source>Installing </source>
        <translation>Installeren</translation>
    </message>
    <message>
        <location filename="work.cpp" line="295"/>
        <source>Could not install </source>
        <translation>Kon niet installeren</translation>
    </message>
    <message>
        <location filename="work.cpp" line="305"/>
        <source>Calculating checksum...</source>
        <translation>Bereken checksum...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="341"/>
        <source>Building new initrd...</source>
        <translation>Nieuwe initrd bouwen...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="412"/>
        <source>Could not create working directory. </source>
        <translation>Kon geen werkmap creëren.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="558"/>
        <source>Calculating total size of excluded files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="work.cpp" line="569"/>
        <source>Calculating size of root...</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>