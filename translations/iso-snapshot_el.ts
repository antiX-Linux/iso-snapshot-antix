<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="el">
<context>
    <name>Batchprocessing</name>
    <message>
        <location filename="batchprocessing.cpp" line="44"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="45"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Ο τρέχων πυρήνας δεν υποστηρίζει τον επιλεγμένο αλγόριθμο συμπίεσης, παρακαλώ επεξεργαστείτε το αρχείο ρυθμίσεων και επιλέξτε διαφορετικό αλγόριθμο.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MX Snapshot</source>
        <translation>MX Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Optional customization</source>
        <translation>Προαιρετική προσαρμογή</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <source>Release date:</source>
        <translation>Ημερομηνία κυκλοφορίας:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>Release codename:</source>
        <translation>Κωδικό όνομα κυκλοφορίας:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Live kernel:</source>
        <translation>Ενεργός πυρήνας:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <source>Boot options:</source>
        <translation>Ρυθμίσεις εκκίνησης</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="139"/>
        <source>Release version:</source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="159"/>
        <source>Change live kernel</source>
        <translation>Αλλαγή ενεργού πυρήνα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>Project name:</source>
        <translation>Όνομα έργου:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot είναι ένα βοηθητικό πρόγραμμα που δημιουργεί μια εικόνα εκκίνησης (ISO) του συστήματος εργασίας σας που μπορείτε να χρησιμοποιήσετε για αποθήκευση ή διανομή. Μπορείτε να συνεχίσετε να εργάζεστε με απαιτητικών εφαρμογών, ενώ βρίσκεται σε λειτουργία.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="205"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Χώρο που χρησιμοποιείται για την / (root) και /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <source>Location and ISO name</source>
        <translation>Τοποθεσία και όνομα ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <source>Snapshot location:</source>
        <translation>τοποθεσία του Snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="249"/>
        <source>Select a different snapshot directory</source>
        <translation>Επιλέξτε ένα διαφορετικό κατάλογο </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <location filename="mainwindow.cpp" line="343"/>
        <source>Snapshot name:</source>
        <translation>όνομα του Snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="379"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /etc/mx-snapshot-exclude.list.</source>
        <translation>Μπορείτε να εξαιρέσετε ορισμένους καταλόγους τσεκάροντας τις επιλογές παρακάτω, ή κάνοντας κλικ στο κουμπί, να επεξεργαστείτε άμεσα το /etc/mx-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="436"/>
        <source>exclude network configurations</source>
        <translation>εξαιρέστε τις διαμορφώσεις δικτύου</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="439"/>
        <source>Networks</source>
        <translation>Δίκτυα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="446"/>
        <source>All of the above</source>
        <translation>Ολα τα παραπανω</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="453"/>
        <source>Pictures</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="470"/>
        <source>Desktop</source>
        <translation>Επιφάνεια εργασίας</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="487"/>
        <source>Music</source>
        <translation>Μουσική</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="494"/>
        <source>Downloads</source>
        <translation>Λήψεις</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="501"/>
        <source>Videos</source>
        <translation>βίντεο</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="508"/>
        <source>Documents</source>
        <translation>&apos;Εγγραφα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="546"/>
        <source>Type of snapshot:</source>
        <translation>Τύπος snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="553"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Διατήρηση λογαριασμών (για προσωπική δημιουργία αντιγράφων ασφαλείας)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="563"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the MX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Αυτή η επιλογή θα επαναφέρει τους κωδικούς &amp;quot;demo&amp;quot; και&amp;quot; root&amp;quot; στις προεπιλογές του MX Linux και δεν θα αντιγράψει τους προσωπικούς λογαριασμούς που δημιουργήθηκαν</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="566"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Επαναφορά λογαριασμούς (για διανομή στους άλλους)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="595"/>
        <source>Edit Exclusion File</source>
        <translation>Επεξεργασία αρχείων Exclude</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>sha512</source>
        <translation>sha512</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <source>Throttle the I/O input rate by the given percentage.</source>
        <translation>Περιορισμός του ρυθμού I/O με το αναφερόμενο ποσοστό.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="663"/>
        <source>I/O throttle:</source>
        <translation>Περιορισμός I/O:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="670"/>
        <source>Calculate checksums:</source>
        <translation>Υπολογίστε αθροίσματα ελέγχου</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="677"/>
        <source>ISO compression scheme:</source>
        <translation>Σύστημα συμπίεσης ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <source>Number of CPU cores to use:</source>
        <translation>Αριθμός πυρήνων CPU που θα χρησιμοποιηθούν:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="691"/>
        <source>md5</source>
        <translation>md5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="701"/>
        <source>Options:</source>
        <translation>Επιλογές:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="848"/>
        <source>About this application</source>
        <translation>Περί εφαρμογής.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="851"/>
        <source>About...</source>
        <translation>Περί</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="858"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="874"/>
        <source>Quit application</source>
        <translation>Κλείστε την εφαρμογή </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="877"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="884"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="916"/>
        <source>Next</source>
        <translation>Επόμενο</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="964"/>
        <source>Back</source>
        <translation>Πίσω</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="981"/>
        <source>Display help </source>
        <translation>Δείτε Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="984"/>
        <source>Help</source>
        <translation>Βοήθεια </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="991"/>
        <source>Alt+H</source>
        <translation>Alt+H </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="173"/>
        <location filename="mainwindow.cpp" line="450"/>
        <source>Snapshot</source>
        <translation>Στιγμιότυπο της εγκατάστασης</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="179"/>
        <source>fastest, worst compression</source>
        <translation>ταχύτερη, χειρότερη συμπίεση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="179"/>
        <source>fast, worse compression</source>
        <translation>γρήγορη, χειρότερη συμπίεση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="180"/>
        <source>slow, better compression</source>
        <translation>αργή, καλύτερη συμπίεση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="180"/>
        <source>best compromise</source>
        <translation>καλύτερος συμβιβασμός</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="181"/>
        <source>slowest, best compression</source>
        <translation>πιο αργή, καλύτερη συμπίεση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Ελεύθερος χώρος στο %1, όπου το φάκελο snapshot τοποθετείται:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="213"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space by removing previous snapshots and saved copies: %1 snapshots are taking up %2 of disk space.</source>
        <translation>Ο ελεύθερος χώρος θα πρέπει να επαρκεί για να χωρέσει τα συμπιεσμένα δεδομένα από τo / (root) και to /home

      Εάν είναι απαραίτητο, μπορείτε να δημιουργήσετε περισσότερο διαθέσιμο χώρο αφαιρώντας προηγούμενα στιγμιότυπα και αποθηκευμένα αντίγραφα: %1 στιγμιότυπα δεσμεύουν %2 χώρου στο δίσκο.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="225"/>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Installing </source>
        <translation>Εγκατάσταση </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Please wait.</source>
        <translation>Παρακαλώ περιμένετε.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Παρακαλώ περιμένετε. Υπολογισμός χώρο που χρησιμοποιείται στο δίσκο ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="327"/>
        <location filename="mainwindow.cpp" line="354"/>
        <location filename="mainwindow.cpp" line="385"/>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="328"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>Το αρχείο εξόδου %1 υπάρχει ήδη. Χρησιμοποιήστε άλλο όνομα αρχείου ή διαγράψτε το υπάρχον αρχείο.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="335"/>
        <source>Settings</source>
        <translation>Ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="340"/>
        <source>Snapshot will use the following settings:</source>
        <translation>Το στιγμιότυπο θα χρησιμοποιήσει τις ακόλουθες ρυθμίσεις:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="342"/>
        <source>- Snapshot directory:</source>
        <translation>- Κατάλογο Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="343"/>
        <source>- Kernel to be used:</source>
        <translation>- Πυρήνα που θα χρησιμοποιηθεί:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Ο τρέχων πυρήνας δεν υποστηρίζει τον επιλεγμένο αλγόριθμο συμπίεσης, παρακαλώ επεξεργαστείτε το αρχείο ρυθμίσεων και επιλέξτε διαφορετικό αλγόριθμο.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="362"/>
        <source>Final chance</source>
        <translation>Τελευταία ευκαιρία</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="364"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot έχει όλες τις πληροφορίες που χρειάζεται για να δημιουργήσει ένα ISO από λειτουργία του συστήματός σας.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Θα πάρει κάποιο χρόνο για να ολοκληρωθεί, ανάλογα με το μέγεθος του εγκατεστημένου συστήματος και την ικανότητα του υπολογιστή σας.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="367"/>
        <source>OK to start?</source>
        <translation>OK για να ξεκινήσετε;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="371"/>
        <source>Shutdown computer when done.</source>
        <translation>Τερματισμός του υπολογιστή όταν τελειώσει.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="385"/>
        <source>Could not create working directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Could not create temporary directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία προσωρινού καταλόγου.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="406"/>
        <source>Output</source>
        <translation>Έξοδος</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Edit Boot Menu</source>
        <translation>Επεξεργασία μενού Boot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="420"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Το πρόγραμμα τώρα θα σταματήσει να σας επιτρέψει να επεξεργαστείτε τα αρχεία στον κατάλογο εργασίας. Επιλέξτε Ναι για να επεξεργαστείτε το μενού εκκίνησης ή επιλέξτε Όχι για να παρακάμψετε αυτό το βήμα και να συνεχίσετε τη δημιουργία του snapshot.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Close</source>
        <translation>Κλείσε</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="441"/>
        <source>Select kernel</source>
        <translation>Επιλογή πυρήνα</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="526"/>
        <source>About %1</source>
        <translation>Περί %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="528"/>
        <source>Version: </source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="529"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Πρόγραμμα για τη δημιουργία ενός live-CD από το σύστημα λειτουργίας για το antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="531"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c)  MX Linux </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="533"/>
        <source>%1 License</source>
        <translation>%1 Άδεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="547"/>
        <source>%1 Help</source>
        <translation>%1 Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="552"/>
        <source>Select Snapshot Directory</source>
        <translation>Επιλέξτε Κατάλογος Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="573"/>
        <source>Confirmation</source>
        <translation>Επιβεβαίωση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="573"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Είστε βέβαιοι ότι θέλετε να τερματίσετε την εφαρμογή</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="70"/>
        <source>License</source>
        <translation>Άδεια</translation>
    </message>
    <message>
        <location filename="about.cpp" line="71"/>
        <location filename="about.cpp" line="81"/>
        <source>Changelog</source>
        <translation>Αρχείο αλλαγών</translation>
    </message>
    <message>
        <location filename="about.cpp" line="72"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="about.cpp" line="94"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="70"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Το πρόγραμμα θα διακόψει τη δημιουργία και θα ανοίξει το μενού εκκίνησης στον επεξεργαστή κειμένου σας.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Εργαλείο που χρησιμοποιείται για τη δημιουργία ενός live-CD από το τρέχον σύστημα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Use CLI only</source>
        <translation>Χρησιμοποιήστε μόνο CLI</translation>
    </message>
    <message>
        <location filename="main.cpp" line="77"/>
        <source>Number of CPU cores to be used.</source>
        <translation>Αριθμός πυρήνων CPU που θα χρησιμοποιηθούν.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Output directory</source>
        <translation>Κατάλογο εξόδου</translation>
    </message>
    <message>
        <location filename="main.cpp" line="79"/>
        <source>Output filename</source>
        <translation>Όνομα αρχείου εξόδου</translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Ονομάστε έναν διαφορετικό πυρήνα για χρήση εκτός από τον προεπιλεγμένο τρέχοντα πυρήνα, χρησιμοποιήστε τη μορφή που επιστράφηκε από &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Or the full path: %1</source>
        <translation>Ή ολόκληρη η διαδρομή: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="86"/>
        <source>Compression level options.</source>
        <translation>Επιλογές επιπέδου συμπίεσης.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>Use quotes: &quot;-Xcompression-level &lt;level&gt;&quot;, or &quot;-Xalgorithm &lt;algorithm&gt;&quot;, or &quot;-Xhc&quot;, see mksquashfs man page</source>
        <translation>Χρησιμοποιήστε εισαγωγικά: &quot;-Compression-level &lt;level&gt; &quot;, ή &quot;-Xalgorithm &lt;algorithm&gt; &quot;, ή &quot;-Xhc&quot;, βλέπε man page mksquashfs</translation>
    </message>
    <message>
        <location filename="main.cpp" line="91"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Δημιουργήστε ένα μηνιαίο στιγμιότυπο, προσθέστε το όνομα &apos;Μήνας&apos; στο όνομα ISO, μην υπολογίζετε τον χρησιμοποιημένο χώρο</translation>
    </message>
    <message>
        <location filename="main.cpp" line="92"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Αυτή η επιλογή ρυθμίζει τους λογαριασμούς επαναφοράς και τη συμπίεση σε προεπιλογές, τα επιχειρήματα που αλλάζουν αυτά τα στοιχεία θα αγνοηθούν</translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Μην υπολογίζετε τα αθροίσματα ελέγχου για το προκύπτον αρχείο ISO</translation>
    </message>
    <message>
        <location filename="main.cpp" line="96"/>
        <source>Skip calculating free space to see if the resulting ISO will fit</source>
        <translation>Παραλείψτε τον υπολογισμό του ελεύθερου χώρου για να δείτε αν ταιριάζει το ISO που προκύπτει</translation>
    </message>
    <message>
        <location filename="main.cpp" line="97"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Επιλογή για επίλυση προβλήματος με τον υπολογισμό των αθροισμάτων ελέγχου στους πυρήνες preempt_rt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Επαναφορά λογαριασμών (για διανομή σε άλλους)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="99"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Υπολογίστε αθροίσματα ελέγχου για το προκύπτον αρχείο ISO</translation>
    </message>
    <message>
        <location filename="main.cpp" line="101"/>
        <source>Throttle the I/O input rate by the given percentage. This can be used to reduce the I/O and CPU consumption of Mksquashfs.</source>
        <translation>Περιορισμός του ρυθμού I/O με το αναφερόμενο ποσοστό. Αυτό μπορεί να χρησιμοποιηθεί για να μειωθεί η χρήση I/O και CPU του Mksquashfs.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <source>Work directory</source>
        <translation>Κατάλογος εργασιών</translation>
    </message>
    <message>
        <location filename="main.cpp" line="106"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Εξαίρεση κύριων φακέλων, έγκυρων επιλογών:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="108"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Χρησιμοποιήστε την επιλογή μία φορά για κάθε στοιχείο που θέλετε να εξαιρέσετε</translation>
    </message>
    <message>
        <location filename="main.cpp" line="111"/>
        <source>Compression format, valid choices: </source>
        <translation>Μορφή συμπίεσης, έγκυρες επιλογές:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="113"/>
        <source>Shutdown computer when done.</source>
        <translation>Τερματισμός του υπολογιστή όταν τελειώσει.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="137"/>
        <source>Snapshot</source>
        <translation>Στιγμιότυπο της εγκατάστασης</translation>
    </message>
    <message>
        <location filename="main.cpp" line="141"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Φαίνεται ότι έχετε συνδεθεί ως root, αποσυνδεθείτε και συνδεθείτε ως κανονικός χρήστης για να χρησιμοποιήσετε αυτό το πρόγραμμα.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="145"/>
        <location filename="main.cpp" line="209"/>
        <location filename="settings.cpp" line="268"/>
        <location filename="settings.cpp" line="283"/>
        <location filename="settings.cpp" line="758"/>
        <location filename="settings.cpp" line="857"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="159"/>
        <source>You must run this program with sudo or pkexec.</source>
        <translation>Πρέπει να τρέξετε αυτό το πρόγραμμα με sudo η pkexec.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="163"/>
        <source>version:</source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="206"/>
        <location filename="settings.cpp" line="277"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Ο τρέχων πυρήνας δεν υποστηρίζει Squashfs, δεν μπορεί να συνεχιστεί.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="88"/>
        <source>Could not create working directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="117"/>
        <source>Could not create temp directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου temp.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="262"/>
        <source>Could not find a usable kernel</source>
        <translation>Δεν ήταν δυνατή η εύρεση ενός χρησιμοποιήσιμου πυρήνα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="386"/>
        <source>Used space on / (root): </source>
        <translation>Χώρος που χρησιμοποιεί το / (root): </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="389"/>
        <source>estimated</source>
        <translation>αναμενόμενα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="398"/>
        <source>Used space on /home: </source>
        <translation>Χώρος που χρησιμοποιεί το /home: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="484"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Ελεύθερος χώρος στο κατάλογο %1, όπου τοποθετείται το snapshot: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="488"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Ο ελεύθερος χώρος πρέπει να είναι επαρκής για να χωρέσει τα συμπιεσμένα δεδομένα από το / και το /home

      Εάν είναι απαραίτητο, μπορείτε να δημιουργήσετε περισσότερο χώρο
      αφαιρώντας προηγούμενα snapshots και αποθηκευμένα αντίγραφα:
      %1 snapshots καταλαμβάνουν %2 χώρο στο δίσκο.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="506"/>
        <source>Desktop</source>
        <translation>Επιφάνεια εργασίας</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="507"/>
        <source>Documents</source>
        <translation>&apos;Εγγραφα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="508"/>
        <source>Downloads</source>
        <translation>Λήψεις</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="509"/>
        <source>Music</source>
        <translation>Μουσική</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="510"/>
        <source>Networks</source>
        <translation>Δίκτυα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="511"/>
        <source>Pictures</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="513"/>
        <source>Videos</source>
        <translation>Βίντεο</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="751"/>
        <location filename="settings.cpp" line="850"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>Το αρχείο εξόδου %1 υπάρχει ήδη. Χρησιμοποιήστε άλλο όνομα αρχείου ή διαγράψτε το υπάρχον αρχείο.</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="82"/>
        <source>Cleaning...</source>
        <translation>Εκκαθάριση...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="93"/>
        <location filename="work.cpp" line="283"/>
        <source>Done</source>
        <translation>Ολοκληρώθηκε</translation>
    </message>
    <message>
        <location filename="work.cpp" line="105"/>
        <source>Interrupted or failed to complete</source>
        <translation>Σταμάτησε ή απέτυχε να ολοκληρωθεί</translation>
    </message>
    <message>
        <location filename="work.cpp" line="138"/>
        <location filename="work.cpp" line="241"/>
        <location filename="work.cpp" line="263"/>
        <location filename="work.cpp" line="301"/>
        <location filename="work.cpp" line="415"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="work.cpp" line="139"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Δεν υπάρχει αρκετός ελεύθερος χώρος στο δίσκο προορισμού σας, χρειάζεστε τουλάχιστον %1</translation>
    </message>
    <message>
        <location filename="work.cpp" line="142"/>
        <source>You have %1 free space on %2</source>
        <translation>Έχετε %1 ελεύθερο χώρο στο %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="145"/>
        <source>If you are sure you have enough free space rerun the program with -o/--override-size option</source>
        <translation>Εάν είστε βέβαιοι ότι έχετε αρκετό ελεύθερο χώρο, εκτελέστε ξανά το πρόγραμμα με την επιλογή -o/--override-size</translation>
    </message>
    <message>
        <location filename="work.cpp" line="170"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Αντιγραφή αρχείων...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="182"/>
        <source>Could not create temp directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου temp.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="238"/>
        <source>Squashing filesystem...</source>
        <translation>Συμπίεση αρχείων...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="242"/>
        <source>Could not create linuxfs file, please check /var/log/%1.log</source>
        <translation>Δεν κατέστη δυνατή η δημιουργία αρχείου linuxfs, παρακαλώ ελέγξτε /var/log/%1.log</translation>
    </message>
    <message>
        <location filename="work.cpp" line="260"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Δημιουργία αρχείου εικόνας CD/DVD...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="264"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Δεν ήταν δυνατή η δημιουργία του αρχείου ISO, παρακαλώ ελέγξτε αν έχετε αρκετό χώρο στο διαμέρισμα προορισμού.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="270"/>
        <source>Making hybrid iso</source>
        <translation>Δημιουργία υβριδικού ISO</translation>
    </message>
    <message>
        <location filename="work.cpp" line="288"/>
        <source>Success</source>
        <translation>Επιτυχία</translation>
    </message>
    <message>
        <location filename="work.cpp" line="289"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Ολοκληρώθηκε με επιτυχία το Snapshot!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="290"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Το snapshot πήρε %1 για να ολοκληρωθεί.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="291"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Ευχαριστούμε που χρησιμοποιήσατε το Snapshot, στη συνέχεια τρέξτε το Live USB Maker!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="298"/>
        <source>Installing </source>
        <translation>Εγκατάσταση </translation>
    </message>
    <message>
        <location filename="work.cpp" line="301"/>
        <source>Could not install </source>
        <translation>Δεν ήταν δυνατή η εγκατάσταση </translation>
    </message>
    <message>
        <location filename="work.cpp" line="311"/>
        <source>Calculating checksum...</source>
        <translation>Υπολογισμός αθροίσματος ...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="350"/>
        <source>Building new initrd...</source>
        <translation>Δημιουργία νέου initrd ...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="416"/>
        <source>Could not create working directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="546"/>
        <source>Calculating total size of excluded files...</source>
        <translation>Υπολογισμός συνολικού μεγέθους των εξαιρούμενων αρχείων...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="556"/>
        <source>Calculating size of root...</source>
        <translation>Υπολογισμός μεγέθους root...</translation>
    </message>
</context>
</TS>